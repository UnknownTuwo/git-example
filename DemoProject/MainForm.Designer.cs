﻿
namespace DemoProject
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnClickThis = new System.Windows.Forms.Button();
			this.lblHelloWorld = new System.Windows.Forms.Label();
			this.ChangeColorButton = new System.Windows.Forms.Button();
			this.btnCountUp = new System.Windows.Forms.Button();
			this.lblCountUp = new System.Windows.Forms.Label();
			this.btnClose = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnClickThis
			// 
			this.btnClickThis.BackColor = System.Drawing.Color.Green;
			this.btnClickThis.Location = new System.Drawing.Point(12, 60);
			this.btnClickThis.Name = "btnClickThis";
			this.btnClickThis.Size = new System.Drawing.Size(81, 25);
			this.btnClickThis.TabIndex = 0;
			this.btnClickThis.Text = "Click this";
			this.btnClickThis.UseVisualStyleBackColor = false;
			this.btnClickThis.Click += new System.EventHandler(this.btnClickThis_Click);
			// 
			// lblHelloWorld
			// 
			this.lblHelloWorld.AutoSize = true;
			this.lblHelloWorld.Location = new System.Drawing.Point(109, 66);
			this.lblHelloWorld.Name = "lblHelloWorld";
			this.lblHelloWorld.Size = new System.Drawing.Size(0, 13);
			this.lblHelloWorld.TabIndex = 1;
			// 
			// ChangeColorButton
			// 
			this.ChangeColorButton.Location = new System.Drawing.Point(12, 31);
			this.ChangeColorButton.Name = "ChangeColorButton";
			this.ChangeColorButton.Size = new System.Drawing.Size(75, 23);
			this.ChangeColorButton.TabIndex = 2;
			this.ChangeColorButton.Text = "button1";
			this.ChangeColorButton.UseVisualStyleBackColor = true;
			this.ChangeColorButton.Click += new System.EventHandler(this.ChangeColorButton_Click);
			// 
			// btnCountUp
			// 
			this.btnCountUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnCountUp.Location = new System.Drawing.Point(12, 110);
			this.btnCountUp.Name = "btnCountUp";
			this.btnCountUp.Size = new System.Drawing.Size(75, 23);
			this.btnCountUp.TabIndex = 2;
			this.btnCountUp.Text = "Count up";
			this.btnCountUp.UseVisualStyleBackColor = false;
			this.btnCountUp.Click += new System.EventHandler(this.btnCountUp_Click);
			// 
			// lblCountUp
			// 
			this.lblCountUp.AutoSize = true;
			this.lblCountUp.Location = new System.Drawing.Point(93, 115);
			this.lblCountUp.Name = "lblCountUp";
			this.lblCountUp.Size = new System.Drawing.Size(13, 13);
			this.lblCountUp.TabIndex = 3;
			this.lblCountUp.Text = "0";
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.btnClose.Location = new System.Drawing.Point(12, 156);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 4;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(315, 191);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.lblCountUp);
			this.Controls.Add(this.btnCountUp);
			this.Controls.Add(this.ChangeColorButton);
			this.Controls.Add(this.lblHelloWorld);
			this.Controls.Add(this.btnClickThis);
			this.Name = "MainForm";
			this.Text = "Demo Project";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClickThis;
        private System.Windows.Forms.Label lblHelloWorld;
		private System.Windows.Forms.Button btnCountUp;
		private System.Windows.Forms.Label lblCountUp;
        private System.Windows.Forms.Button ChangeColorButton;
		private System.Windows.Forms.Button btnClose;
	}
}

