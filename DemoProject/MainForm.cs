﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoProject
{
    public partial class MainForm : Form
    {
		#region Variables/Properties
		private int countUp = 0;
		#endregion

		#region Constructors
		public MainForm()
        {
            InitializeComponent();
        }
		#endregion

		#region EventHandler
		private void btnClickThis_Click(object sender, EventArgs e)
        {
            lblHelloWorld.Text = "Hello World!";
        }

        private void ChangeColorButton_Click(object sender, EventArgs e)
        {
            ChangeColorButton.BackColor = Color.Yellow;
        }

		private void btnCountUp_Click(object sender, EventArgs e)
		{
			lblCountUp.Text = (++countUp).ToString();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		#endregion
	}
}
